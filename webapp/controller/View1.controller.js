sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/resource/ResourceModel"
], function(Controller, MessageToast, ResourceModel) {
	"use strict";

	return Controller.extend("nnext.iq.HelloWorld.controller.View1", {
		ShowMessage: function() {
    		var tMsg=this.getView().getModel("i18n").getResourceBundle().getText("helloTest");
    		MessageToast.show(tMsg);
		}
	});
});